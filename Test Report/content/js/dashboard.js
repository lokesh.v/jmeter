/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 75.60975609756098, "KoPercent": 24.390243902439025};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.5714285714285714, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-5"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-4"], "isController": false}, {"data": [0.5, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout"], "isController": false}, {"data": [0.8, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-1"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/time-at-work?timezoneOffset=5.5&currentDate=2023-01-22&currentTime=22:33"], "isController": false}, {"data": [0.5, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-0"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-0"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-3"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-1"], "isController": false}, {"data": [0.2, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-2"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-2"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/subunit"], "isController": false}, {"data": [0.5, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-3"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-4"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-5"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-6"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/buzz/feed?limit=5&offset=0&sortOrder=DESC&sortField=share.createdAtUtc"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/core/i18n/messages"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/locations"], "isController": false}, {"data": [0.5, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-5"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-4"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/action-summary"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-6"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/shortcuts"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-1"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-0"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-3"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-2"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-1"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-2"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-0"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-5"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-6"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-3"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-4"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/leaves?date=2023-01-22"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 205, 50, 24.390243902439025, 754.5560975609757, 161, 7549, 290.0, 2409.0000000000023, 3727.3999999999996, 6485.839999999998, 5.238813217142419, 1084.8559466120212, 4.84138514700621], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/login", 5, 0, 0.0, 5496.4, 4582, 7549, 4992.0, 7549.0, 7549.0, 7549.0, 0.6035003017501509, 2540.9025865645744, 2.052136768255884], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-5", 5, 0, 0.0, 4075.8, 3207, 5304, 3705.0, 5304.0, 5304.0, 5304.0, 0.8670019074041964, 1274.9737190046817, 0.49615538841685447], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-4", 5, 0, 0.0, 4308.6, 3108, 6536, 3693.0, 6536.0, 6536.0, 6536.0, 0.7140816909454442, 1094.8287933358326, 0.4156178591830906], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout", 5, 0, 0.0, 827.6, 783, 918, 799.0, 918.0, 918.0, 918.0, 0.28992230082337933, 3.1022818697089183, 1.2890783551258265], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-1", 5, 0, 0.0, 379.6, 179, 710, 183.0, 710.0, 710.0, 710.0, 7.042253521126761, 11.51243397887324, 4.043794014084507], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/time-at-work?timezoneOffset=5.5&currentDate=2023-01-22&currentTime=22:33", 5, 5, 100.0, 288.8, 279, 298, 286.0, 298.0, 298.0, 298.0, 0.2875711738655317, 0.09211264162880313, 0.16821790346235693], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-0", 5, 0, 0.0, 1066.2, 994, 1232, 1013.0, 1232.0, 1232.0, 1232.0, 2.865329512893983, 12.404862106017191, 1.4718391833810887], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-0", 5, 0, 0.0, 335.6, 286, 491, 302.0, 491.0, 491.0, 491.0, 0.2884005306569764, 0.46724265660148817, 0.22869260829439927], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-3", 5, 0, 0.0, 2752.4, 2221, 3737, 2652.0, 3737.0, 3737.0, 3737.0, 1.1896264572924102, 774.359692853319, 0.6831058172733762], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-1", 5, 0, 0.0, 335.4, 307, 408, 319.0, 408.0, 408.0, 408.0, 0.2867959160261558, 1.2486265164334058, 0.17560648373867155], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-2", 5, 0, 0.0, 2073.0, 1025, 3207, 1900.0, 3207.0, 3207.0, 3207.0, 1.3823610727121924, 759.7896543233343, 0.8072772670721593], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-2", 5, 0, 0.0, 242.2, 210, 276, 245.0, 276.0, 276.0, 276.0, 0.28866693608914035, 0.27936419303158017, 0.20381464335200045], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/subunit", 5, 5, 100.0, 328.4, 278, 465, 302.0, 465.0, 465.0, 465.0, 0.28604118993135014, 0.09162256864988558, 0.14860733695652173], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate", 5, 0, 0.0, 916.6, 842, 1111, 867.0, 1111.0, 1111.0, 1111.0, 0.2784429470401515, 3.0134379176644206, 1.3813380575820013], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-3", 5, 0, 0.0, 241.6, 208, 275, 245.0, 275.0, 275.0, 275.0, 0.28868360277136257, 0.2799441577655889, 0.20720942191108543], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-4", 5, 0, 0.0, 241.8, 209, 275, 245.0, 275.0, 275.0, 275.0, 0.28866693608914035, 0.27992799564112925, 0.20437844596154955], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-5", 5, 0, 0.0, 242.0, 210, 275, 245.0, 275.0, 275.0, 275.0, 0.28866693608914035, 0.2802098969459038, 0.2069155577045205], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-6", 5, 0, 0.0, 242.0, 210, 275, 245.0, 275.0, 275.0, 275.0, 0.28866693608914035, 0.2802098969459038, 0.204096544656775], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/buzz/feed?limit=5&offset=0&sortOrder=DESC&sortField=share.createdAtUtc", 5, 5, 100.0, 336.2, 291, 456, 295.0, 456.0, 456.0, 456.0, 0.28659864725438494, 0.09180112919867017, 0.14357920511865183], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/core/i18n/messages", 15, 15, 100.0, 299.93333333333334, 284, 315, 302.0, 310.2, 315.0, 315.0, 0.4370629370629371, 0.4490138767482517, 0.2266410347465035], "isController": false}, {"data": ["Test", 5, 5, 100.0, 11166.6, 10085, 12965, 10744.0, 12965.0, 12965.0, 12965.0, 0.3636099192785979, 1544.5465471829323, 7.843889762562723], "isController": true}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/locations", 5, 5, 100.0, 314.4, 281, 408, 293.0, 408.0, 408.0, 408.0, 0.2858449576949462, 0.09155971301166246, 0.1490636791104505], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push", 5, 0, 0.0, 876.0, 792, 1006, 844.0, 1006.0, 1006.0, 1006.0, 0.277069710739222, 2.961669570126344, 0.9724497464812147], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-5", 5, 0, 0.0, 206.4, 167, 257, 202.0, 257.0, 257.0, 257.0, 0.2863852454321553, 0.27799505269488517, 0.14990477690589382], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-4", 5, 0, 0.0, 209.0, 176, 256, 202.0, 256.0, 256.0, 256.0, 0.2863852454321553, 0.27771537960364284, 0.14738771908471276], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/action-summary", 5, 5, 100.0, 287.6, 284, 294, 286.0, 294.0, 294.0, 294.0, 0.2876042565429968, 0.09212323842392867, 0.1513854436295657], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-6", 5, 0, 0.0, 206.2, 161, 257, 202.0, 257.0, 257.0, 257.0, 0.2863852454321553, 0.27799505269488517, 0.1471080459934704], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/shortcuts", 5, 5, 100.0, 290.2, 284, 298, 289.0, 298.0, 298.0, 298.0, 0.2875215641173088, 0.09209675100632547, 0.1471301753881541], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-1", 5, 0, 0.0, 354.0, 305, 417, 322.0, 417.0, 417.0, 417.0, 0.2851765242685222, 1.2035897486168938, 0.11947336807733987], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-0", 5, 0, 0.0, 308.6, 278, 392, 289.0, 392.0, 392.0, 392.0, 0.2857959416976279, 0.46302291726207484, 0.14345616604744213], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-3", 5, 0, 0.0, 208.2, 167, 257, 202.0, 257.0, 257.0, 257.0, 0.2863852454321553, 0.27771537960364284, 0.15018444999713615], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-2", 5, 0, 0.0, 204.8, 166, 257, 202.0, 257.0, 257.0, 257.0, 0.2863852454321553, 0.27715603342115813, 0.14682837290222808], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-1", 5, 0, 0.0, 339.4, 314, 409, 320.0, 409.0, 409.0, 409.0, 0.29827596492274655, 1.262196690628169, 0.16836280051303465], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-2", 5, 0, 0.0, 176.4, 165, 202, 170.0, 202.0, 202.0, 202.0, 0.3025901718712176, 0.2928387307855241, 0.19916579671992252], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-0", 5, 0, 0.0, 300.8, 288, 329, 294.0, 329.0, 329.0, 329.0, 0.2984540082373306, 0.48353046842356595, 0.16875475661075628], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-5", 5, 0, 0.0, 176.4, 161, 201, 174.0, 201.0, 201.0, 201.0, 0.30253524535608395, 0.29367190808979243, 0.20237953424698976], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-6", 5, 0, 0.0, 179.0, 165, 201, 178.0, 201.0, 201.0, 201.0, 0.3027000847560237, 0.2938319182104371, 0.19953374727569922], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-3", 5, 0, 0.0, 183.2, 176, 201, 178.0, 201.0, 201.0, 201.0, 0.3024986387561256, 0.29334096512190694, 0.20265045526045133], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-4", 5, 0, 0.0, 181.6, 168, 201, 181.0, 201.0, 201.0, 201.0, 0.30253524535608395, 0.2933764635142494, 0.1997205330671023], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/leaves?date=2023-01-22", 5, 5, 100.0, 304.6, 281, 353, 299.0, 353.0, 353.0, 353.0, 0.28646728543600325, 0.09175905236621977, 0.15302500501317748], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["Test failed: code expected to contain /200/", 15, 30.0, 7.317073170731708], "isController": false}, {"data": ["401/Unauthorized", 35, 70.0, 17.073170731707318], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 205, 50, "401/Unauthorized", 35, "Test failed: code expected to contain /200/", 15, "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/time-at-work?timezoneOffset=5.5&currentDate=2023-01-22&currentTime=22:33", 5, 5, "401/Unauthorized", 5, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/subunit", 5, 5, "401/Unauthorized", 5, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/buzz/feed?limit=5&offset=0&sortOrder=DESC&sortField=share.createdAtUtc", 5, 5, "401/Unauthorized", 5, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/core/i18n/messages", 15, 15, "Test failed: code expected to contain /200/", 15, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/locations", 5, 5, "401/Unauthorized", 5, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/action-summary", 5, 5, "401/Unauthorized", 5, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/shortcuts", 5, 5, "401/Unauthorized", 5, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/leaves?date=2023-01-22", 5, 5, "401/Unauthorized", 5, "", "", "", "", "", "", "", ""], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
